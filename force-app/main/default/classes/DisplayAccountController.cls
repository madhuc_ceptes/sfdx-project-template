public class DisplayAccountController {
    @auraEnabled
    public list<Account> getAccounts(){
        List<Account> accList;
        accList = [SELECT Id,Name FROM Account];
        return accList;        
    }
}